package notas.appnotas;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import notas.appnotas.models.Topico;

public class AppnotasTopicoTests {

	@Test
	public void testeNomeTopico() {
		Topico topico = new Topico("Topico comidas");
		Topico topicoResult = new Topico("Topico comidas");
		assertEquals(topico.getNome(), topicoResult.getNome());
	}
}
