package notas.appnotas;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import notas.appnotas.models.Item;

@SpringBootTest
class AppnotasItemTests {

	@Test
	public void testeItemTitulo() {
		Item item = new Item(1,"titolo aproved", "O neymar é brabo");
		Item itemResult = new Item(1,"titolo aproved", "O neymar é brabo");
		assertEquals(itemResult.getTitulo(), item.getTitulo());
	}
	
	@Test
	public void testeItemConteudo() {
		Item item = new Item(1,"titolo aproved", "O neymar é brabo");
		Item itemResult = new Item(1,"titolo aproved", "O neymar é brabo");
		assertEquals(itemResult.getConteudo(), item.getConteudo());
	}
	
	

}
