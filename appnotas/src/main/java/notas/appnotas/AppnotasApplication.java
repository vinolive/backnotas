package notas.appnotas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppnotasApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppnotasApplication.class, args);
	}

}
