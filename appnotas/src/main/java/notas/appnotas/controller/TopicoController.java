package notas.appnotas.controller;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import notas.appnotas.dto.TopicoDTO;
import notas.appnotas.form.TopicoForm;
import notas.appnotas.models.Topico;
import notas.appnotas.repository.TopicoRepoistory;


@RestController
@RequestMapping("/topicos")
public class TopicoController {
	@Autowired TopicoRepoistory topicoRepository;
	@CrossOrigin
	@GetMapping
	public List<TopicoDTO>listar(){
		List<Topico> topicos=topicoRepository.findAll();
		return TopicoDTO.converterAll(topicos);
	}
	@GetMapping("/{id}")
	public TopicoDTO busca(@PathVariable int id){
		Optional<Topico> topico=topicoRepository.findById(id);
		return TopicoDTO.converter(topico.get());
	}
	
	@CrossOrigin
	@PostMapping
    @Transactional
    public Topico salvar(@RequestBody @Valid TopicoForm topicoForm) {
        Topico topico= topicoForm.converter();
        topicoRepository.save(topico);
        return topico;
    }
	
	@CrossOrigin
	@DeleteMapping("/{id}")
	public String delete(@PathVariable int id) {
		this.topicoRepository.deleteById(id);
		return "deletado com sucesso";
	}
	
}
