package notas.appnotas.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import notas.appnotas.models.Item;
import notas.appnotas.repository.ItemRepository;

@RestController
@RequestMapping("/item")
public class ItemController {
	
	
	@Autowired
	private ItemRepository itemRepository;
	
	@GetMapping
	public List<Item> todosItens () {
		List<Item> item = itemRepository.findAll();		
			return item;
	}
	
	@PostMapping
	public Item salvarItem(@RequestBody Item item) {
		itemRepository.save(item);		
		return item;
	}
	
}
