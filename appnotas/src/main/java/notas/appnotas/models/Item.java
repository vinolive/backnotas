package notas.appnotas.models;

import javax.persistence.*;

@Entity
public class Item {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private int idItem;
	private int idTopico;
	private String titulo;
	private String conteudo;
	
	public Item() {}

	public Item(int idTopico,String titulo, String conteudo) {
		this.idTopico = idTopico;
		this.titulo = titulo;
		this.conteudo = conteudo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	
	public int getIdItem() {
		return idItem;
	}

	
	
	public void setIdItem(int idItem) {
		this.idItem = idItem;
	}

	public int getIdTopico() {
		return idTopico;
	}

	public void setIdTopico(int idTopico) {
		this.idTopico = idTopico;
	}
	
	
	
	
}
