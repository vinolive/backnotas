package notas.appnotas.models;

import javax.persistence.*;

@Entity
public class Topico {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private int idTopico;
	private String nome;
	
	public Topico() {
		
	}

	public Topico(String nome) {
		this.nome = nome;
	}

	public int getIdTopico() {
		return idTopico;
	}

	public void setIdTopico(int idTopico) {
		this.idTopico = idTopico;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	
	
}
