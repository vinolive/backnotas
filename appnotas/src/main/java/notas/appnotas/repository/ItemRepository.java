package notas.appnotas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import notas.appnotas.models.Item;

@Repository
public interface ItemRepository extends JpaRepository<Item, Integer>{

}
