package notas.appnotas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import notas.appnotas.models.Topico;

@Repository
public interface TopicoRepoistory extends JpaRepository<Topico, Integer>{

}
