package notas.appnotas.form;
import javax.validation.constraints.*;
import notas.appnotas.models.Item;

public class ItemForm {

	@NotNull @NotBlank 
	private String titulo;
	private int idTopico;
	private String conteudo;
	
	
	
	public ItemForm(@NotNull @NotBlank String titulo, int idTopico, String conteudo) {
		this.titulo = titulo;
		this.idTopico = idTopico;
		this.conteudo = conteudo;
	}

	public int getIdTopico() {
		return idTopico;
	}

	public void setIdTopico(int idTopico) {
		this.idTopico = idTopico;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getConteudo() {
		return conteudo;
	}
	
	
	
	public Item converter() {
		return new Item(this.idTopico,this.titulo, this.conteudo);
	}
}
