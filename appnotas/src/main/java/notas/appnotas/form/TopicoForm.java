package notas.appnotas.form;

import javax.validation.constraints.*;
import notas.appnotas.models.Topico;

public class TopicoForm {

	//Bean Validation
	
	@NotNull @NotEmpty @NotBlank 
	private int idTopico;
	@NotNull @NotBlank 
	private String nome;
	
	public int getIdTopico() {
		return idTopico;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setIdTopico(int idTopico) {
		this.idTopico = idTopico;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Topico converter() {
		return new Topico(this.nome);
	}
}
