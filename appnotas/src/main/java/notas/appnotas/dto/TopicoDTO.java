package notas.appnotas.dto;

import java.util.List;
import java.util.stream.Collectors;

import notas.appnotas.models.Topico;

public class TopicoDTO {

	private String nome;

	public TopicoDTO(Topico topico) {
		this.nome = topico.getNome();
	}

	public String getNome() {
		return nome;
	}
	
	public static List<TopicoDTO> converterAll(List<Topico> topicos){
		return topicos.stream().map(TopicoDTO::new).collect(Collectors.toList());
	}
	
	public static TopicoDTO converter(Topico topico) {
		return new TopicoDTO(topico);
	}
	
}
