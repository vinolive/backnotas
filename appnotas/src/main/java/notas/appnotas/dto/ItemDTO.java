package notas.appnotas.dto;

import java.util.List;
import java.util.stream.Collectors;

import notas.appnotas.models.Item;

public class ItemDTO {

	
	private String titulo;
	private String conteudo;

	public ItemDTO(Item item) {
		this.titulo = item.getTitulo();
		this.conteudo = item.getConteudo();
	}

	
	public String getTitulo() {
		return titulo;
	}
	
	public String getConteudo() {
		return conteudo;
	}

	public static List<ItemDTO> converter(List<Item> itens){
		return itens.stream().map(ItemDTO::new).collect(Collectors.toList());
	}
	
	public static ItemDTO converter(Item item) {
		return new ItemDTO(item);
	}
}
